package de.ghostium.hsbremerhaven.projects.life.diseases;

public interface IDisease {
  double chanceOfInfection();

  double chanceOfDecease();

  double chanceOfCure();
}

package de.ghostium.hsbremerhaven.projects.life;

import de.ghostium.hsbremerhaven.projects.life.stone.Stone;

import javax.swing.*;
import java.awt.*;

public class StoneRenderer extends JComponent {
  private Stone[][] grid;
  public static final int RECT_SIZE = 16;

  public StoneRenderer(Stone[][] grid) {
    this.grid = grid;
  }

  @Override
  protected void paintComponent(Graphics graphics) {
    for (int y = 0; y < grid.length; ++y) {
      for (int x = 0; x < grid[y].length; ++x) {
        graphics.setColor(grid[y][x].getColorRepresentation());
        graphics.fillRect(x * RECT_SIZE, y * RECT_SIZE, RECT_SIZE, RECT_SIZE);
      }
    }
  }
}

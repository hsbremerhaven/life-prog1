package de.ghostium.hsbremerhaven.projects.life.diseases;

public class HIV implements IDisease {

  @Override
  public double chanceOfInfection() {
    return 0.05;
  }

  @Override
  public double chanceOfDecease() {
    return 0.9;
  }

  @Override
  public double chanceOfCure() {
    return 0.1;
  }

  @Override
  public String toString() {
    return "HIV";
  }
}

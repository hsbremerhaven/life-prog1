package de.ghostium.hsbremerhaven.projects.life.stone;

import java.awt.*;
import java.io.DataOutputStream;
import java.io.IOException;

public class AlwaysStone extends ImmutableStone {
  public AlwaysStone() {
    super(1);
  }

  @Override
  public void print() {
    System.out.print("   #   ");
  }

  @Override
  public Color getColorRepresentation() {
    return Color.GREEN;
  }

  @Override
  public void save(DataOutputStream dos) throws IOException {
    dos.writeByte(3);
  }
}

package de.ghostium.hsbremerhaven.projects.life.diseases;

public class Tetanus implements IDisease {
  @Override
  public double chanceOfInfection() {
    return 0.4;
  }

  @Override
  public double chanceOfDecease() {
    return 0.37;
  }

  @Override
  public double chanceOfCure() {
    return 0.5;
  }

  @Override
  public String toString() {
    return "Tetanus";
  }
}

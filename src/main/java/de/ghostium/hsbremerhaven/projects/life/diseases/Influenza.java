package de.ghostium.hsbremerhaven.projects.life.diseases;

public class Influenza implements IDisease {

  @Override
  public double chanceOfInfection() {
    return 0.8;
  }

  @Override
  public double chanceOfDecease() {
    return 0.05;
  }

  @Override
  public double chanceOfCure() {
    return 0.7;
  }

  @Override
  public String toString() {
    return "Influenza";
  }
}

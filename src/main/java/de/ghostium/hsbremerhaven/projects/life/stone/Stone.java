package de.ghostium.hsbremerhaven.projects.life.stone;

import de.ghostium.hsbremerhaven.projects.life.diseases.HIV;
import de.ghostium.hsbremerhaven.projects.life.diseases.IDisease;
import de.ghostium.hsbremerhaven.projects.life.diseases.Influenza;
import de.ghostium.hsbremerhaven.projects.life.diseases.Tetanus;

import java.awt.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;


public class Stone {

  int lifeDuration;
  int nextLifeDuration;
  Stone[] neighbors;
  IDisease[] diseases;
  IDisease[] nextDiseases;
  final static int[] DISEASES_MASK = {1 << 2, 1 << 3, 1 << 4};

  public Stone(int lifeDuration) {
    this.lifeDuration = lifeDuration;
    this.diseases = new IDisease[3];
    if (Math.random() < 0.1) {
      double infectionRand = Math.random();
      if (infectionRand <= 1D / 3D) {
        diseases[0] = new Influenza();
        System.out.println("FLU");
      } else if (infectionRand <= 2D / 3D) {
        diseases[1] = new HIV();
        System.out.println("HIV");
      } else {
        diseases[2] = new Tetanus();
        System.out.println("Teta");
      }
    }
  }

  public Stone(byte binaryStoneRep, DataInputStream dis) throws IOException {
    this.lifeDuration = dis.readInt();
    this.diseases = new IDisease[3];
    if ((DISEASES_MASK[0] & binaryStoneRep) != 0) diseases[0] = new Influenza();
    if ((DISEASES_MASK[1] & binaryStoneRep) != 0) diseases[1] = new HIV();
    if ((DISEASES_MASK[2] & binaryStoneRep) != 0) diseases[2] = new Tetanus();
  }

  public Color getColorRepresentation() {
    int blackStrengh = 255 - (63 * lifeDuration);
    if (blackStrengh < 0) blackStrengh = 0;
    return new Color(blackStrengh, blackStrengh, blackStrengh);
  }

  public void setupNeighbors(Stone[][] grid, int y, int x) {
    this.neighbors = new Stone[8];
    int neighborNumber = 0;
    for (int offsetY = -1; offsetY <= 1; ++offsetY) {
      for (int offsetX = -1; offsetX <= 1; ++offsetX) {
        int neighborY = (offsetY + y + grid.length) % grid.length;
        int neighborX = (offsetX + x + grid[neighborY].length) % grid[neighborY].length;
        if (!(neighborY == y && neighborX == x)) {
          neighbors[neighborNumber++] = grid[neighborY][neighborX];
        }
      }
    }
  }

  public void print() {
    char fieldRepresentation;
    String prefix = "   ";
    String suffix = "   ";
    switch (lifeDuration) {
      case 0:
        fieldRepresentation = ' ';
        break;
      case 1:
        fieldRepresentation = '.';
        break;
      case 2:
        fieldRepresentation = 'o';
        break;
      case 3:
        fieldRepresentation = 'O';
        break;
      default:
        fieldRepresentation = '*';
        break;
    }
    if (isAlive()) { //Some empty cells are initialized with diseases -> Prog Üb
      if (diseases[0] != null) {
        prefix = "  (";
        suffix = ")  ";
      }
      if (diseases[1] != null) {
        prefix = " {" + prefix.charAt(2);
        suffix = suffix.charAt(0) + "} ";
      }
      if (diseases[2] != null) {
        prefix = "[" + prefix.substring(1);
        suffix = suffix.substring(0, 2) + "]";
      }
    }
    System.out.print(prefix + fieldRepresentation + suffix);
  }

  public int countNeighbour() {
    if (this.neighbors == null) {
      throw new IllegalStateException("Stone method was used without setting neighbors.");
    }
    int livingNeighborsAmount = 0;
    for (Stone neighbor : neighbors) {
      if (neighbor.isAlive()) {
        ++livingNeighborsAmount;
      }
    }
    return livingNeighborsAmount;
  }

  public boolean isAlive() {
    return lifeDuration > 0;
  }

  public IDisease[] getDiseases(double infectionValue) {
    IDisease[] diseasesWhoSpread = new IDisease[diseases.length];
    int pos = 0;
    for (int i = 0; i < diseases.length; i++) {
      if (diseases[i] != null && infectionValue < diseases[i].chanceOfInfection()) {
        diseasesWhoSpread[pos++] = diseases[i];
      } else {
        diseasesWhoSpread[pos++] = null;
      }
    }
    return diseasesWhoSpread;
  }

  public int getLifeDuration() {
    return lifeDuration;
  }

  public boolean infectedWithInfluenza() {
    return diseases[0] != null;
  }

  public boolean infectedWithHIV() {
    return diseases[1] != null;
  }

  public boolean infectedWithTetanus() {
    return diseases[2] != null;
  }

  public void save(DataOutputStream dos) throws IOException {
    byte binaryStoneRep = 0;
    for (int i = 0; i < diseases.length; i++) {
      if (diseases[i] != null) {
        binaryStoneRep |= DISEASES_MASK[i];
      }
    }
    dos.writeByte(binaryStoneRep);
    dos.writeInt(lifeDuration);
  }

  public void computeNext() {
    nextDiseases = new IDisease[diseases.length];
    for (int i = 0; i < neighbors.length; i++) {
      IDisease[] spreadedDiseases = neighbors[i].getDiseases(Math.random());
      for (int j = 0; j < diseases.length; j++) {
        if (spreadedDiseases[j] != null && diseases[j] == null) {
          nextDiseases[j] = spreadedDiseases[j];
        }
      }
    }
    for (int i = 0; i < diseases.length; i++) {
      if (diseases[i] != null && Math.random() < diseases[i].chanceOfCure()) {
        nextDiseases[i] = null;
        return;
      }
    }
    for (int i = 0; i < diseases.length; i++) {
      if (diseases[i] != null && Math.random() < diseases[i].chanceOfDecease()) {
        nextLifeDuration = 0;
        nextDiseases = new IDisease[diseases.length];
        return;
      }
    }
    if ((countNeighbour() == 2 && lifeDuration > 0) || (countNeighbour() == 3)) {
      nextLifeDuration = lifeDuration + 1;
    } else {
      nextLifeDuration = 0;
      nextDiseases = new IDisease[diseases.length];
    }
  }

  public void commitNext() {
    lifeDuration = nextLifeDuration;
    diseases = nextDiseases;
  }
}



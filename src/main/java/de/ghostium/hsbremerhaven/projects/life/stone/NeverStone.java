package de.ghostium.hsbremerhaven.projects.life.stone;

import java.awt.*;
import java.io.DataOutputStream;
import java.io.IOException;

public class NeverStone extends ImmutableStone {
  public NeverStone() {
    super(0);
  }

  @Override
  public void print() {
    System.out.print("   X   ");
  }

  @Override
  public Color getColorRepresentation() {
    return Color.RED;
  }

  @Override
  public void save(DataOutputStream dos) throws IOException {
    dos.writeByte(1);
  }

}

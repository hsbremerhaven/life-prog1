package de.ghostium.hsbremerhaven.projects.life;

import de.ghostium.hsbremerhaven.projects.life.stone.AlwaysStone;
import de.ghostium.hsbremerhaven.projects.life.stone.NeverStone;
import de.ghostium.hsbremerhaven.projects.life.stone.Stone;

import javax.swing.*;
import java.io.*;

public class Life {
  public static void main(String[] args) throws InterruptedException, IOException {
    int gridHeight = 50;
    int gridWidth = 50;
    Stone[][] lifeGrid = createRandomGrid(gridHeight, gridWidth);
    printGrid(lifeGrid);
    System.out.println();
    int ticksToTake = 400;
    JFrame jFrame = createGUI(lifeGrid);
    for (int i = 0; i <= ticksToTake; ++i) {
      simulateLife(lifeGrid);
      printGrid(lifeGrid);
      jFrame.repaint();
      System.out.println();
      Thread.sleep(1000 / 25);
    }

    /*System.out.println("Speicher-Lade Test");
    printGrid(lifeGrid);
    System.out.println();
    safeLife(lifeGrid);
    lifeGrid = loadLife();
    printGrid(lifeGrid);

  400000
    for (int[] a : findFieldsWithXLivingNeighbors(lifeGrid, 4)) {
      System.out.println(a[0] + ", " + a[1]);
    }
    System.out.println();
    for (int[] a : findFieldsWithXLivingNeighborsRecursive(lifeGrid, 4)) {
      System.out.println(a[0] + ", " + a[1]);
    }*/
  }

  private static JFrame createGUI(Stone[][] grid) {
    JFrame jFrame = new JFrame("Life (42)");
    int jFrameWidth = grid[0].length * StoneRenderer.RECT_SIZE;
    int jFrameHeight = (grid.length * StoneRenderer.RECT_SIZE) + 27;
    int screenWidth = jFrame.getToolkit().getScreenSize().width;
    int screenHeight = jFrame.getToolkit().getScreenSize().height;
    jFrame.setBounds((screenWidth / 2) - (jFrameWidth / 2), (screenHeight / 2) - (jFrameHeight / 2), jFrameWidth, jFrameHeight);
    jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jFrame.add(new StoneRenderer(grid));
    jFrame.setVisible(true);
    return jFrame;
  }

  private static void printGrid(Stone[][] grid) {
    for (int y = 0; y < grid.length; ++y) {
      for (int x = 0; x < grid[y].length; ++x) {
        grid[y][x].print();
      }
      System.out.println();
    }
  }

  public static void simulateLife(Stone[][] grid) {
    for (int y = 0; y < grid.length; ++y) {
      for (int x = 0; x < grid[y].length; ++x) {
        grid[y][x].computeNext();
      }
    }
    for (int y = 0; y < grid.length; ++y) {
      for (int x = 0; x < grid[y].length; ++x) {
        grid[y][x].commitNext();
      }
    }
  }

  public static Stone[][] createGrid(int gridHeight, int gridWidth) {
    Stone[][] grid = new Stone[gridHeight][gridWidth];
    for (int y = 0; y < grid.length; ++y) {
      for (int x = 0; x < grid[y].length; ++x) {
        grid[y][x] = new Stone((x % (y + 2) == 0) ? 1 : 0);
      }
    }
    for (int y = 0; y < grid.length; ++y) {
      for (int x = 0; x < grid[y].length; ++x) {
        grid[y][x].setupNeighbors(grid, y, x);
      }
    }
    return grid;
  }

  public static Stone[][] createRandomGrid(int gridHeight, int gridWidth) {
    Stone[][] grid = new Stone[gridHeight][gridWidth];
    for (int y = 0; y < grid.length; ++y) {
      for (int x = 0; x < grid[y].length; ++x) {
        double random = Math.random();
        if (random <= 0.05) {
          grid[y][x] = new AlwaysStone();
        } else if (random <= 0.1) {
          grid[y][x] = new NeverStone();
        } else {
          grid[y][x] = new Stone((random >= 0.55) ? 1 : 0);
        }
      }
    }
    for (int y = 0; y < grid.length; ++y) {
      for (int x = 0; x < grid[y].length; ++x) {
        grid[y][x].setupNeighbors(grid, y, x);
      }
    }
    return grid;
  }

  public static Stone[][] loadLife() throws IOException {
    File f = new File("life.grid");
    DataInputStream dis = null;
    Stone[][] grid;
    try {
      dis = new DataInputStream(new FileInputStream(f));
      int gridHeight = dis.readInt();
      int gridWidth = dis.readInt();
      grid = new Stone[gridHeight][gridWidth];
      for (int y = 0; y < gridHeight; ++y) {
        for (int x = 0; x < gridWidth; ++x) {
          byte binaryStoneRep = dis.readByte();
          Stone stone = null;
          boolean flagIsSpecialStone = (binaryStoneRep & 0b00000001) > 0;
          if (binaryStoneRep == 3) {
            stone = new AlwaysStone();
          } else if (binaryStoneRep == 1) {
            stone = new NeverStone();
          } else {
            stone = new Stone(binaryStoneRep, dis);
          }
          grid[y][x] = stone;
        }
      }
    } finally {
      if (dis != null) dis.close();
    }
    return grid;
  }

  public static void safeLife(Stone[][] grid) throws IOException {
    /*
    Format:
    int - gridHeight
    int - gridWidth
    per stone:
    byte - 00000000
                  | Normal Stone ; Special Stone
                 | Never Stone ; Always Stone
                | Not infected with Influenza ; Influenza
               | Not ; HIV
              | Not; Tetanus
     int - Lifetime
     */
    File f = new File("life.grid");
    DataOutputStream dos = null;
    try {
      dos = new DataOutputStream(new FileOutputStream(f));
      dos.writeInt(grid.length);
      dos.writeInt(grid[0].length);
      for (int y = 0; y < grid.length; ++y) {
        for (int x = 0; x < grid[y].length; ++x) {
          Stone stone = grid[y][x];
          stone.save(dos);
        }
      }
    } finally {
      if (dos != null) dos.close();
    }
  }

  public static int[][] findFieldsWithXLivingNeighborsRecursive(Stone[][] grid, int amountWanted) {
    //Method overloading alternative
    return __findFieldsWithXLivingNeighborsRecursive(grid, amountWanted, 0, 0, 0);
  }

  private static int[][] __findFieldsWithXLivingNeighborsRecursive(Stone[][] grid, int amountWanted, int y, int x, int amountsFound) {
    if (y == grid.length) {
      return new int[amountsFound][2];
    } else {
      int nextY, nextX;
      if (x == grid[0].length - 1) {
        nextY = y + 1;
        nextX = 0;
      } else {
        nextY = y;
        nextX = x + 1;
      }
      boolean qualifiedField = grid[y][x].countNeighbour() == amountWanted;
      int[][] result = __findFieldsWithXLivingNeighborsRecursive(grid, amountWanted, nextY, nextX, qualifiedField ? amountsFound + 1 : amountsFound);
      if (qualifiedField)
        result[amountsFound] = new int[]{y, x};
      return result;
    }
  }

  public static int[][] findFieldsWithXLivingNeighbors(Stone[][] grid, int amountWanted) {
    int fieldPosition = 0;
    for (int currentY = 0; currentY < grid.length; ++currentY) {
      for (int currentX = 0; currentX < grid[currentY].length; ++currentX) {
        int livingNeighbors = grid[currentY][currentX].countNeighbour();
        if (livingNeighbors == amountWanted) {
          fieldPosition++;
        }
      }
    }
    int[][] fields = new int[fieldPosition][2];
    fieldPosition = 0;
    for (int currentY = 0; currentY < grid.length; ++currentY) {
      for (int currentX = 0; currentX < grid[currentY].length; ++currentX) {
        int livingNeighbors = grid[currentY][currentX].countNeighbour();
        if (livingNeighbors == amountWanted) {
          fields[fieldPosition++] = new int[]{currentY, currentX};
        }
      }
    }
    return fields;
  }
}


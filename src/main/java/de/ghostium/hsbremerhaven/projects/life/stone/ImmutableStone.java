package de.ghostium.hsbremerhaven.projects.life.stone;

import de.ghostium.hsbremerhaven.projects.life.diseases.IDisease;

public class ImmutableStone extends Stone {

  public ImmutableStone(int immutableLifeDuration) {
    super(immutableLifeDuration);
    this.diseases = new IDisease[3];
  }

  @Override
  public void computeNext() {
  }

  @Override
  public void commitNext() {
  }


}
